from django.shortcuts import redirect, render
from receipts.models import ExpenseCategory, Account, Receipt
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm
from django.contrib.auth.decorators import login_required

# Create your views here.

@login_required
def receipts_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts_list": receipts,
    }

    return render(request, "receipts/list.html", context)

@login_required
def category_list(request):
    category = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category_list": category,
    }

    return render(request, "receipts/categories.html", context)

@login_required
def account_list(request):
    account = Account.objects.filter(owner=request.user)
    context = {
        "account_list": account,
    }

    return render(request, "receipts/accounts.html", context)



@login_required
def create_receipts(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)
            recipe.purchaser = request.user
            recipe.save()

            return redirect("home")

    else:
        form = ReceiptForm

    context = {
        "form": form,
    }

    return render(request, "receipts/create.html", context)

@login_required
def create_categories(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()

            return redirect("category_list")

    else:
        form = ExpenseCategoryForm

    context = {
        "form":form,
    }

    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()

            return redirect("account_list")

    else:
        form = AccountForm

    context = {
        "form": form
    }

    return render(request, "receipts/create_account.html", context)
