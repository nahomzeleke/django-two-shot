from django.urls import path
from receipts.views import receipts_list, create_receipts, category_list, account_list, create_categories, create_account

urlpatterns = [
    path("", receipts_list, name="home"),
    path("create/", create_receipts, name="create_receipt"),
    path("categories/", category_list, name="category_list"),
    path("accounts/", account_list, name="account_list"),
    path("categories/create/", create_categories, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),



]
